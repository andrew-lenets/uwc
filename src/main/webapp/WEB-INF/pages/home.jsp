<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html; charset=utf-8" %>
<tiles:insertDefinition name="template"/>
<tiles:putAttribute name="body"/>

<script type="text/javascript" src="/resources/pages/home.js"></script>
<div style="width: 500px;margin: 0 auto; ">
    <form id="generate">
        <label for="url">Enter url:</label><br/>
        <input type="text" id="url" placeholder="http://example.com" style="width: 300px;"/> <br/><br/>
        <label for="url">Enter max links per sitemap:
            <input type="number" id="max_links" min="1" max="100000" placeholder="50000" style="width: 80px;"/>
            <br/><br/>
        </label>
        <button type="submit">Generate</button>
        <button id="stop">Stop</button>
    </form>
    <div id="last_sitemap"></div>
    <div>
        <input id="is_started" type="hidden" value="">
        <div id="stats"></div>
    </div>
    <div id="prev">
        <h2>Previous sitemaps</h2>

        <div id="prev_sitemaps"></div>
    </div>

</div>
