$(document).ready(function () {
    $("#is_started").val(false);
    getPrev();
    $('#generate').submit(function (e) {
        e.preventDefault();
        generate();
    });

    $("#stop").click(function (e) {
        e.preventDefault()
        $.ajax({
            type: "GET",
            url: "/home/stop",
            //async: false,
            success: function () {
                $("#is_started").val(false);
            }
        });
    });

    $.ajax({
        url: '/home/isWorking',
        type: "GET",
        beforeSend: function () {
        },
        success: function (response) {
            var isS = $("#is_started");
            isS.val(response);
        }
    });
});

function generate() {
    var url = $("#url").val();
    var urlReg = /(^https?:\/\/[\wа-яА-Я.ёЁ-]+\.[а-яА-Яa-zA-Z\.]{2,})\/?/g;
    var match = urlReg.exec(url);
    if (match == null) {
        alert("Enter correct url");
        return;
    }
    url = match[1];
    var isStarted = $("#is_started");
    isStarted.val(true);
    var maxLinks = $('#max_links').val();
    $.ajax({
        type: "POST",
        url: "/home/generate",
        //async: false,
        data: {
            "url": url,
            "maxLinks": maxLinks
        },
        success: function (response) {
            var last = $("#last_sitemap");
            var name = response.name;
            var link = "/sitemaps/" + response.link;
            var path = window.location.pathname;
            var file = (path + link).replace("//", "/");
            last.html('<a href="' + file + '" onclick="rem()">' + name + '</a>');
            isStarted.val(false);
            $('#url').val('');
        },
        error: function () {
            isStarted.val(false);
        }
    });
}

function rem() {
    $('#last_sitemap').html('');
    getPrev();
}

setInterval(function () {
    var isStarted = $("#is_started");
    var div = $("#stats");
    if (isStarted.attr("value") == "true") {
        $.ajax({
            type: "GET",
            url: "/home/getStats",
            success: function (response) {
                var percent = Math.round(response.processedLinks / response.allLinks * 10000) / 100;
                div.html("");
                for (var key in response) {
                    div.append(key + ": " + response[key] + (key == 'level' ? (', ' + percent + '%') : '') + "<br>");
                }
            }
        });
    } else {
        div.html("");
    }
}, 500);

function getPrev() {
    $.ajax({
        type: "GET",
        url: "/home/getAllSitemaps",
        success: function (response) {
            var div = $("#prev_sitemaps");
            div.html("");
            $.each(response, function (index, value) {
                var name = value.name;
                var link = "/sitemaps/" + value.link;
                var path = window.location.pathname;
                var file = (path + link).replace("//", "/");
                div.append(value.date + ' <a href="' + file + '">' + name + '</a><br>');
            });
        }
    });
}