package com.sitemap.bo;

import org.springframework.stereotype.Component;

/**
 * mocker on 26.03.15 at 0:46.
 */
@Component
public class HttpStatusCodeChecker {
    public boolean isRedirect(int statusCode) {
        switch (statusCode) {
            case 301:
            case 302:
            case 303:
            case 307:
                return true;
        }
        return false;
    }

    public boolean isOK(int statusCode) {
        switch (statusCode) {
            case 200:
            case 304:
                return true;
        }
        return false;
    }

    public boolean isNotFound(int statusCode) {
        return statusCode == 404;
    }

    public boolean isMethodNotAllowed(int statusCode) {
        return statusCode == 405;
    }

    public boolean isBadRequest(int statusCode) {
        return statusCode == 400;
    }
}
