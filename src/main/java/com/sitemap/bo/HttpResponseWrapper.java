package com.sitemap.bo;

import org.apache.http.HttpResponse;

/**
 * Created by andrew on 23.03.15.
 */
public class HttpResponseWrapper {
    private HttpResponse httpResponse;
    private String content;

    public HttpResponseWrapper() {

    }

    public HttpResponseWrapper(HttpResponse httpResponse, String content) {
        this.httpResponse = httpResponse;
        this.content = content;
    }

    public HttpResponse getHttpResponse() {
        return httpResponse;
    }

    public void setHttpResponse(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
