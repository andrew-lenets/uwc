package com.sitemap.bo;

import java.io.File;
import java.util.Date;

/**
 * mocker on 26.03.15 at 15:01.
 */
public class SitemapInfo {
    private String name;
    private File file;
    private Date date;

    public SitemapInfo() {
    }

    public SitemapInfo(String name, File file, Date date) {
        this.name = name;
        this.file = file;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
