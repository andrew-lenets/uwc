package com.sitemap.bo;

/**
 * Created by andrew on 27.03.15.
 */
public enum ChangeFreq {
    ALWAYS, HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY, NEVER, NONE;
    String lowerCase;

    private ChangeFreq() {
        lowerCase = this.name().toLowerCase();
    }

    @Override
    public String toString() {
        return lowerCase;
    }
}