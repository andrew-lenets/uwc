package com.sitemap.bo.sitemap;

import com.sitemap.utils.EncodeUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by andrew on 27.03.15.
 */
@XmlRootElement(name = "urlset")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sitemap {

    @XmlElement(name = "url")
    private List<Link> links = new LinkedList<>();

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public void addLink(Link link) {
        link.setUrl(EncodeUtils.escapeCodes(link.getUrl()));
        links.add(link);
    }

    public void clear() {
        links.clear();
    }
}
