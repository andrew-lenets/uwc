package com.sitemap.bo.sitemap;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * mocker on 29.03.15 at 12:20.
 */
@XmlRootElement(name = "sitemapindex")
@XmlAccessorType(XmlAccessType.FIELD)
public class SitemapIndex {

    @XmlElement(name = "sitemap")
    private List<SitemapLoc> sitemaps = new ArrayList<>();

    public void addLoc(SitemapLoc loc) {
        sitemaps.add(loc);
    }
}


