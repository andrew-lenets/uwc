package com.sitemap.bo.sitemap;

import com.sitemap.bo.ChangeFreq;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * mocker on 28.03.15 at 0:23.
 */
public class ChangeFreqAdapter extends XmlAdapter<String, ChangeFreq> {
    @Override
    public ChangeFreq unmarshal(String v) throws Exception {
        return null;
    }

    @Override
    public String marshal(ChangeFreq v) throws Exception {
        return v.toString();
    }
}
