/**
 * Created by andrew on 27.03.15.
 */
@XmlSchema(
        namespace = "http://www.sitemaps.org/schemas/sitemap/0.9",
        elementFormDefault = XmlNsForm.QUALIFIED
)
@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(
                type = ChangeFreq.class, value = ChangeFreqAdapter.class
        )
}) package com.sitemap.bo.sitemap;

import com.sitemap.bo.ChangeFreq;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;