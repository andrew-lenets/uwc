package com.sitemap.bo.sitemap;


import com.sitemap.bo.ChangeFreq;
import com.sitemap.bo.DateConstants;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

/**
 * mocker on 25.03.15 at 23:18.
 */

@XmlAccessorType(XmlAccessType.FIELD)
public class Link {

    @XmlElement(name = "loc")
    private String url;

    @XmlElement(name = "lastmod")
    private Date lastModified = null;

    @XmlElement(name = "changefreq")
    private ChangeFreq changeFreq = null;

    @XmlElement(name = "priority")
    private Double priority = null;

    @XmlTransient
    private boolean afterRedirect;

    public Link(String url) {
        this(url, null);
    }

    public Link(String url, Date lastModified) {
        this.url = url.replaceAll("#.*", "");
        this.url = this.url.replaceAll("/?\\??$", "");
        this.lastModified = lastModified;
    }

    public boolean isEmpty() {
        return url.isEmpty();
    }

    public boolean hasDomain() {
        return url.startsWith("http");
    }

    public boolean hasDomain(Link link) {
        return url.startsWith(link.getUrl());
    }

    public void calculateOptions() {
        priority = calculatePriority();
        if (lastModified == null) {
            return;
        }
        changeFreq = calculateChangeFreq();
    }

    private ChangeFreq calculateChangeFreq() {
        // ALWAYS, HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY, NEVER;
        long[] changeFreq = {DateConstants.HOUR,  /*less hour*/
                2 * DateConstants.HOUR,                /*hour - day*/
                2 * DateConstants.DAY,               /*day - week*/
                2 * DateConstants.WEEK,              /*week - month*/
                2 * DateConstants.MONTH,               /*month - year*/
                2 * DateConstants.YEAR,           /*year - 2 years*/
                DateConstants.INF                 /*more than 2 years*/};

        long diff = new Date().getTime() - lastModified.getTime();
        for (int i = 0; i < changeFreq.length; i++) {
            if (diff < changeFreq[i]) {
                return ChangeFreq.values()[i];
            }
        }
        return null;
    }

    private double calculatePriority() {
        int level = url.replaceAll("[^/]", "").length() - 2;
        return ((int) (Math.max(1.0 - 0.2 * level, 0) * 100)) / 100.0;
    }

    public void setDomain(Link link) {
        url = link.getUrl() + "/" + url;
        url = url.replaceAll("([^:])//", "$1/");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? "" : url;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public ChangeFreq getChangeFreq() {
        return changeFreq;
    }

    public void setChangeFreq(ChangeFreq changeFreq) {
        this.changeFreq = changeFreq;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public boolean isAfterRedirect() {
        return afterRedirect;
    }

    public void setAfterRedirect(boolean afterRedirect) {
        this.afterRedirect = afterRedirect;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Link))
            return false;
        Link link = (Link) obj;
        return this.getUrl().equals(link.getUrl());
    }

    @Override
    public String toString() {
        return url;
    }
}
