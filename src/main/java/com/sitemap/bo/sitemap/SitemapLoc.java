package com.sitemap.bo.sitemap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.Date;

/**
 * mocker on 29.03.15 at 12:32.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SitemapLoc {
    @XmlElement(name = "loc")
    private String loc;

    @XmlElement(name = "lastmod")
    private Date lastModified = null;

    public SitemapLoc() {
    }

    public SitemapLoc(String loc) {
        this.loc = loc;
    }

    public SitemapLoc(String loc, Date lastModified) {
        this.loc = loc;
        this.lastModified = lastModified;
    }
}
