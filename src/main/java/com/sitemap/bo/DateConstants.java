package com.sitemap.bo;

/**
 * mocker on 27.03.15 at 4:45.
 */
public class DateConstants {
    public static final long MIN = 60 * 1000;
    public static final long HOUR = 60 * MIN;
    public static final long DAY = 24 * HOUR;
    public static final long WEEK = 7 * DAY;
    public static final long MONTH = 30 * DAY;
    public static final long YEAR = 365 * DAY;
    public static final long INF = Long.MAX_VALUE;
}
