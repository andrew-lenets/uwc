package com.sitemap.exception;

/**
 * mocker on 24.03.15 at 1:04.
 */
public class NotAPageException extends Exception {
    public NotAPageException(String message) {
        super(message);
    }
}
