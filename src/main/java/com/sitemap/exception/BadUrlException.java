package com.sitemap.exception;

/**
 * mocker on 24.03.15 at 0:42.
 */
public class BadUrlException extends Exception {
    public BadUrlException(String message) {
        super(message);
    }
}
