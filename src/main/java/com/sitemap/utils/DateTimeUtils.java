package com.sitemap.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * mocker on 26.03.15 at 14:51.
 */
public class DateTimeUtils {
    private static final Logger LOGGER = LogManager.getLogger();

    public static Date getDateTime() {
        return new Date();
    }

    public static String dateTimeToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    public static String getDateTimeForName() {
        return convertDateTimeForName(getDateTime());
    }

    public static String convertDateTimeForName(Date date) {
        String dateTime = dateTimeToString(date);
        dateTime = dateTime.replaceAll("[/:\\.]", "");
        dateTime = dateTime.replaceAll("\\s", "_");
        return dateTime;
    }

    // Fri, 23 Jan 2015 14:22:18 GMT ==> Thu Mar 26 19:35:24 EET 2015
    public static Date convertDate(String date) {
        DateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        try {
            return format.parse(date);
        } catch (ParseException e) {
            LOGGER.error("can not parse date: " + date);
        }
        return new Date();
    }
}
