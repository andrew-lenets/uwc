package com.sitemap.utils;

import com.sitemap.bo.HttpResponseWrapper;
import com.sitemap.bo.sitemap.Link;
import com.sitemap.exception.BadUrlException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpRequestWrapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * mocker on 23.03.15 at 12:51.
 */
@Component
@Scope("prototype")
public class HttpRequestUtils {
    private final Logger LOGGER = LogManager.getLogger();
    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/41.0.2228.0 Safari/537.36";
    private final int MAX_TIMEOUT = 3 * 1000;
    private HttpContext context;

    public HttpResponseWrapper headResponse(Link link) throws BadUrlException {
        try {
            String url = encode(link);
            HttpRequestBase headRequest = new HttpHead(url);
            HttpResponseWrapper response = response(headRequest);
            postProcessing(link, context);
            return response;
        } catch (URISyntaxException | IOException e) {
            throw new BadUrlException("Bad url: " + link);
        }
    }

    public HttpResponseWrapper getResponse(Link link) throws BadUrlException {
        try {
            String url = encode(link);
            HttpRequestBase getRequest = new HttpGet(url);
            HttpResponseWrapper response = response(getRequest);
            postProcessing(link, context);
            return response;
        } catch (URISyntaxException | IOException e) {
            throw new BadUrlException("Bad url: " + link);
        }
    }

    public HttpResponseWrapper response(HttpRequestBase request) throws IOException, URISyntaxException {
        String html = "";
        try (CloseableHttpClient httpClient = createHttpClient()) {
            context = new BasicHttpContext();
            HttpResponse response = httpClient.execute(request, context);
            LOGGER.info("parsing " + request.getURI().toString() + " " + request.getMethod() + " " + response.getStatusLine().getStatusCode());
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                html = EntityUtils.toString(entity, "UTF-8");
            }
            return new HttpResponseWrapper(response, html);
        } catch (IOException e) {
            LOGGER.error("unable to get response: " + request.getURI());
            throw e;
        }
    }

    private CloseableHttpClient createHttpClient() {
        return HttpClientBuilder
                .create()
                .setUserAgent(USER_AGENT)
                .setDefaultRequestConfig(RequestConfig
                        .custom()
                        .setSocketTimeout(MAX_TIMEOUT)
                        .setConnectTimeout(MAX_TIMEOUT)
                        .build())
                        //.disableRedirectHandling()
                .build();
    }

    private void postProcessing(Link link, HttpContext context) {
        Object attribute = context.getAttribute("http.request");
        if (attribute != null) {
            String uri = ((HttpRequestWrapper) attribute).getOriginal().getRequestLine().getUri();
            Link newLink = new Link(uri);
            if (!link.equals(newLink)) {
                link.setUrl(newLink.getUrl());
                link.setAfterRedirect(true);
            }
        }
    }

    private String encode(Link link) throws URISyntaxException {
        return EncodeUtils.encodeToUTF8(link.getUrl());
    }
}
