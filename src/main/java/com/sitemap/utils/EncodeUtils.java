package com.sitemap.utils;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by andrew on 26.03.15.
 */
public class EncodeUtils {
    public static String encodeToUTF8(String url) throws URISyntaxException {
        url = url.replaceAll("\\s", "%20");
        //url = escapeCodes(url);
        return new URI(url).toASCIIString();
    }

    public static String escapeCodes(String url) {
        String[][] codes = {{"&", "&amp;"},
                {"'", "&apos;"},
                {"\"", "&quot;"},
                {">", "&gt;"},
                {"<", "&lt;"}};
        for (String[] code : codes) {
            url = url.replaceAll(code[0] + "&(?:.{3}[^;]|.{0,2}$)", code[1]);
        }
        return url;
    }
}
