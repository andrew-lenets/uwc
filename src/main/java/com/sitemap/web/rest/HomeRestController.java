package com.sitemap.web.rest;

import com.sitemap.bo.SitemapInfo;
import com.sitemap.dto.SitemapInfoDto;
import com.sitemap.generator.Generator;
import com.sitemap.parser.Parser;
import com.sitemap.parser.ParserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * mocker on 23.03.15 at 12:46.
 */
@Controller
@Scope("session")
@RequestMapping("/home")
public class HomeRestController {
    private final String URL_PATTERN = "(^https?://[\\wа-яА-Я.ёЁ-]+\\.[а-яА-Яa-zA-Z\\.]{2,})/?";

    @Autowired
    private ParserStatus parserStatus;

    @Autowired
    private Generator generator;

    @Autowired
    private Parser parser;

    @Autowired
    private ServletContext servletContext;

    @ResponseBody
    @RequestMapping(value = "/generate", method = RequestMethod.POST)
    public SitemapInfoDto generateSitemap(@RequestParam("url") String url,
                                          @RequestParam(value = "maxLinks",
                                                  required = false,
                                                  defaultValue = "50000") Integer maxLinks) {
        if (!url.matches(URL_PATTERN)) {
            return null;
        }

        generator.setPath(servletContext.getRealPath("/WEB-INF"));
        generator.setMaxLinks(maxLinks);
        generator.generate(url);

        List<SitemapInfo> sitemapInfos = generator.getSitemapInfos();
        return new SitemapInfoDto(sitemapInfos.get(sitemapInfos.size() - 1));
    }

    @ResponseBody
    @RequestMapping(value = "/stop", method = RequestMethod.GET)
    public void stop() throws Exception {
        parser.setWorking(false);
    }

    @ResponseBody
    @RequestMapping(value = "/isWorking", method = RequestMethod.GET)
    public boolean isWorking() throws Exception {
        return parser.isWorking();
    }

    @ResponseBody
    @RequestMapping(value = "/getStats", method = RequestMethod.GET)
    public ParserStatus getStat() {
        return parserStatus;
    }

    @ResponseBody
    @RequestMapping(value = "/getAllSitemaps", method = RequestMethod.GET)
    public List<SitemapInfoDto> getAllSitemaps() {
        List<SitemapInfo> sitemapInfos = generator.getSitemapInfos();
        List<SitemapInfoDto> sitemapInfoDtos = new LinkedList<>();
        ListIterator<SitemapInfo> it = sitemapInfos.listIterator(sitemapInfos.size());
        while (it.hasPrevious()) {
            SitemapInfo sitemapInfo = it.previous();
            sitemapInfoDtos.add(new SitemapInfoDto(sitemapInfo));
        }
        return sitemapInfoDtos;
    }

    @RequestMapping(value = "/sitemaps/{name}", method = RequestMethod.GET)
    public void downloadSitemap(@PathVariable(value = "name") String name,
                                HttpServletResponse res) throws IOException {
        List<SitemapInfo> sitemapInfos = generator.getSitemapInfos();
        for (SitemapInfo sitemapInfo : sitemapInfos) {
            if (sitemapInfo.getFile().getName().startsWith(name)) {
                File file = sitemapInfo.getFile();

                res.setContentType("text/xml");
                res.setContentLength(new Long(file.length()).intValue());
                res.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
                FileCopyUtils.copy(new FileInputStream(file), res.getOutputStream());
            }
        }
    }
}
