package com.sitemap.web.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * mocker on 23.03.15 at 1:10.
 */

@Controller
public class HomeController {

    @RequestMapping(value = {"/home"}, method = RequestMethod.GET)
    public String homePage() {
        return "home";
    }
}