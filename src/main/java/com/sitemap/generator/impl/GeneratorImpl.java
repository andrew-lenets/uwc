package com.sitemap.generator.impl;

import com.sitemap.bo.SitemapInfo;
import com.sitemap.bo.sitemap.Link;
import com.sitemap.bo.sitemap.Sitemap;
import com.sitemap.bo.sitemap.SitemapIndex;
import com.sitemap.bo.sitemap.SitemapLoc;
import com.sitemap.generator.Generator;
import com.sitemap.parser.Parser;
import com.sitemap.utils.DateTimeUtils;
import com.sitemap.utils.ZipUtils;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * mocker on 26.03.15 at 13:43.
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class GeneratorImpl implements Generator {
    private final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private Parser parser;

    @Autowired
    private ZipUtils zipUtils;

    private String path;
    private List<SitemapInfo> sitemapInfos = new LinkedList<>();
    private List<Link> parsedLinks;
    private int maxLinks = 50000;
    private List<SitemapLoc> sitemapLocs = new ArrayList<>();
    private List<File> files = new ArrayList<>();

    @Override
    public void generate(String url) {
        parser.init();
        sitemapLocs.clear();
        files.clear();
        parser.parse(url);
        LOGGER.info("parser finished. found " + parser.getParsedLinks().size());
        parsedLinks = parser.getParsedLinks();

        Link domain = parser.getDomain();
        Sitemap sitemap = new Sitemap();

        Marshaller marshaller = buildMarshaller(Sitemap.class);
        if (marshaller == null) {
            return;
        }

        String fullPath = "", fileName = "";
        int index = 0;
        while (index < parsedLinks.size()) {
            sitemap.clear();
            fileName = DateTimeUtils.convertDateTimeForName(new Date());
            fullPath = getFullFilePath(fileName);
            int cnt = 0;
            for (; index < parsedLinks.size() && cnt < maxLinks; cnt++, index++) {
                sitemap.addLink(parsedLinks.get(index));
            }
            saveSitemap(marshaller, sitemap, fullPath);
            sitemapLocs.add(new SitemapLoc(getSitemapLoc(fileName, parser.getDomain()), new Date()));
        }

        if (sitemapLocs.size() > 1) {
            fileName = "sitemap_index";
            fullPath = getFullFilePath(fileName);
            createSitemapIndex(fullPath);
        }

        SitemapInfo sitemapInfo = createSitemapInfo(domain, fullPath, new Date(), sitemapLocs.size() > 1);
        sitemapInfos.add(sitemapInfo);
    }

    public List<SitemapInfo> getSitemapInfos() {
        return sitemapInfos;
    }

    private void createSitemapIndex(String fullPath) {
        Marshaller marshaller = buildMarshaller(SitemapIndex.class);
        if (marshaller == null) {
            return;
        }
        SitemapIndex sitemapIndex = new SitemapIndex();
        for (SitemapLoc sitemapLoc : sitemapLocs) {
            sitemapIndex.addLoc(sitemapLoc);
        }
        saveSitemap(marshaller, sitemapIndex, fullPath);
    }

    private String getSitemapLoc(String fileName, Link domain) {
        return (domain.getUrl() + "/" + fileName).replaceAll("[^:]//", "/") + ".xml";
    }

    private String getFullFilePath(String fileName) {
        return path + "/" + fileName + ".xml";
    }

    private Marshaller buildMarshaller(Class clazz) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Marshaller marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            marshaller.setProperty("com.sun.xml.bind.characterEscapeHandler", new CharacterEscapeHandler() {
                @Override
                public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
                    out.write(ch, start, length);
                }
            });
            return marshaller;
        } catch (JAXBException e) {
            LOGGER.error("can not create marshaller");
        }
        return null;
    }

    private void saveSitemap(Marshaller marshaller, Object jaxbElement, String fullPath) {
        File file = new File(fullPath);
        files.add(file);
        try {
            marshaller.marshal(jaxbElement, file);
        } catch (Exception e) {
            LOGGER.error("can not write file");
        }
    }

    private SitemapInfo createSitemapInfo(Link domain, String fullPath, Date date, boolean isGzip) {
        String name = domain.getUrl().replaceAll("https?://", "");

        if (isGzip) {
            name += " (gzip)";
            fullPath = fullPath.replaceAll("\\.xml$", ".zip");
            zipUtils.gzip(files, fullPath);
        }

        File sitemapFile = new File(fullPath);
        return new SitemapInfo(name, sitemapFile, date);
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path + "/sitemaps";
    }

    public void setMaxLinks(int maxLinks) {
        this.maxLinks = maxLinks;
    }
}
