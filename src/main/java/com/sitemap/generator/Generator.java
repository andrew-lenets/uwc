package com.sitemap.generator;

import com.sitemap.bo.SitemapInfo;

import java.util.List;

/**
 * mocker on 26.03.15 at 13:43.
 */
public interface Generator {
    void generate(String url);

    String getPath();

    void setPath(String path);

    List<SitemapInfo> getSitemapInfos();

    void setMaxLinks(int maxLinks);
}
