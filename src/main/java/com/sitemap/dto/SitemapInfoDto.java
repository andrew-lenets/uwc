package com.sitemap.dto;

import com.sitemap.bo.SitemapInfo;
import com.sitemap.utils.DateTimeUtils;

/**
 * mocker on 28.03.15 at 17:19.
 */
public class SitemapInfoDto {
    private String name;
    private String link;
    private String date;

    public SitemapInfoDto() {
    }

    public SitemapInfoDto(SitemapInfo sitemapInfo) {
        name = sitemapInfo.getName();
        link = sitemapInfo.getFile().getName().replaceAll("\\.xml$", "");
        date = DateTimeUtils.dateTimeToString(sitemapInfo.getDate());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
