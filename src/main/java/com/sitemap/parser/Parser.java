package com.sitemap.parser;

import com.sitemap.bo.sitemap.Link;

import java.util.List;

/**
 * mocker on 23.03.15 at 23:37.
 */
public interface Parser {
    void init();

    void parse(String url);

    List<Link> getParsedLinks();

    Link getDomain();

    void setWorking(boolean isInterrupted);

    public boolean isWorking();
}
