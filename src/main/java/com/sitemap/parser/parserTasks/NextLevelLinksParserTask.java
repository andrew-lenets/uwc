package com.sitemap.parser.parserTasks;

import com.sitemap.bo.HttpResponseWrapper;
import com.sitemap.bo.sitemap.Link;
import com.sitemap.exception.BadUrlException;
import com.sitemap.parser.ParserStatus;
import com.sitemap.utils.DateTimeUtils;
import com.sitemap.utils.HttpRequestUtils;
import org.apache.http.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * mocker on 26.03.15 at 10:11.
 */
@Component
@Scope("prototype")
public class NextLevelLinksParserTask implements Runnable {
    private final Pattern hrefPattern = Pattern.compile("<a[^>]+?href\\s*=\\s*[\"'](.*?)[\"']", Pattern.DOTALL);
    private Link currentLink;
    private List<Link> nextLevelLinks;

    @Autowired
    private ParserStatus parserStatus;

    @Autowired
    private HttpRequestUtils httpRequestUtils;

    @Override
    public void run() {
        HttpResponseWrapper response;
        parserStatus.setCurrentUrl(currentLink.getUrl());
        try {
            response = httpRequestUtils.getResponse(currentLink);
            Date lastModified = getLastModified(response);
            currentLink.setLastModified(lastModified);
            currentLink.calculateOptions();
        } catch (BadUrlException e) {
            return;
        }
        List<String> allUrls = getAllUrls(response);
        for (String url : allUrls) {
            nextLevelLinks.add(new Link(url));
        }
        parserStatus.addAllLinks(allUrls.size());
    }

    private Date getLastModified(HttpResponseWrapper response) {
        Header dateHeader = response.getHttpResponse().getFirstHeader("Last-Modified");
        return dateHeader == null ? null : DateTimeUtils.convertDate(dateHeader.getValue());
    }

    private List<String> getAllUrls(HttpResponseWrapper response) {
        String html = response.getContent();
        List<String> urls = new ArrayList<>();

        Matcher m = hrefPattern.matcher(html);
        while (m.find()) {
            urls.add(m.group(1));
        }

        return urls;
    }

    public void setCurrentLink(Link currentLink) {
        this.currentLink = currentLink;
    }

    public void setNextLevelLinks(List<Link> nextLevelLinks) {
        this.nextLevelLinks = nextLevelLinks;
    }


}
