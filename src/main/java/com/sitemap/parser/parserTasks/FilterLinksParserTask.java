package com.sitemap.parser.parserTasks;

import com.sitemap.bo.HttpStatusCodeChecker;
import com.sitemap.bo.sitemap.Link;
import com.sitemap.exception.BadUrlException;
import com.sitemap.parser.ParserStatus;
import com.sitemap.utils.HttpRequestUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * mocker on 26.03.15 at 10:39.
 */
@Component
@Scope("prototype")
public class FilterLinksParserTask implements Runnable {
    private Link currentLink;
    private List<Link> filterLinks;
    private Set<String> usedUrls;
    private Link domain;
    private boolean checkForDomain;

    @Autowired
    private ParserStatus parserStatus;

    @Autowired
    private HttpRequestUtils httpRequestUtils;

    @Autowired
    private HttpStatusCodeChecker httpStatusCodeChecker;

    @Override
    public void run() {
        parserStatus.addProcessedLinks(1);
        if (currentLink == null) {
            return;
        }

        if (!isCorrectLink(currentLink)) {
            return;
        }

        if (currentLink.getUrl().startsWith("//")) {
            currentLink.setUrl("http:" + currentLink.getUrl());
        }

        if (!currentLink.hasDomain()) {
            currentLink.setDomain(domain);
        } else if (!currentLink.hasDomain(domain)) {
            return;
        }

        if (isCorrectPage(currentLink) && (!currentLink.isAfterRedirect() || isCorrectLink(currentLink))) {
            filterLinks.add(currentLink);
            parserStatus.addFoundLinks(1);
        }
    }

    public void setDomain(Link domain) {
        this.domain = domain;
    }

    public void setFilterLinks(List<Link> filterLinks) {
        this.filterLinks = filterLinks;
    }

    public void setCurrentLink(Link currentLink) {
        this.currentLink = currentLink;
    }

    public void setUsedUrls(Set<String> usedUrls) {
        this.usedUrls = usedUrls;
    }

    public void setCheckForDomain(boolean checkForDomain) {
        this.checkForDomain = checkForDomain;
    }

    private boolean isCorrectLink(Link link) {
        return !(currentLink.isEmpty() ||
                isUsed(link) ||
                (checkForDomain && currentLink.equals(domain)));
    }

    private boolean isUsed(Link link) {
        String urlCutPattern = "^https?://[\\wа-яА-Я.ёЁ-]+\\.[а-яА-Яa-zA-Z\\.]{2,}/?(.*)";
        String url = link.getUrl().replaceAll(urlCutPattern, "$1");
        url = url.replaceAll("^/", "");
        if (usedUrls.contains(url)) {
            return true;
        } else {
            usedUrls.add(url);
            return false;
        }
    }

    private boolean isCorrectPage(Link link) {

        try {
            HttpResponse response = httpRequestUtils.headResponse(link).getHttpResponse();
            int statusCode = response.getStatusLine().getStatusCode();

            if (!httpStatusCodeChecker.isOK(statusCode) && !httpStatusCodeChecker.isNotFound(statusCode)) {
                response = httpRequestUtils.getResponse(link).getHttpResponse();
                statusCode = response.getStatusLine().getStatusCode();
            }

            if (!httpStatusCodeChecker.isOK(statusCode)) {
                return false;
            }

            Header firstHeader = response.getFirstHeader("Content-Type");
            return (firstHeader.getValue().toLowerCase().contains("text/html"));

        } catch (BadUrlException e) {
            return false;
        }
    }
}
