package com.sitemap.parser.impl;

import com.sitemap.bo.sitemap.Link;
import com.sitemap.parser.Parser;
import com.sitemap.parser.ParserStatus;
import com.sitemap.parser.parserTasks.FilterLinksParserTask;
import com.sitemap.parser.parserTasks.NextLevelLinksParserTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * mocker on 23.03.15 at 13:25.
 */

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class ParserImpl implements Parser {
    private final Logger LOGGER = LogManager.getLogger();

    private Link domain;
    private List<Link> parsedLinks = new ArrayList<>();
    private Set<String> usedUrls = new HashSet<>();
    private boolean isWorking = false;

    @Autowired
    private ParserStatus parserStatus;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void init() {
        parserStatus.init();
        parsedLinks.clear();
        usedUrls.clear();
        isWorking = true;
    }

    @Override
    public void parse(String url) {
        domain = new Link(url);
        Link rootLink = new Link(url);
        parsedLinks.add(rootLink);
        parserStatus.setCurrentUrl(url);
        filterLinks(Arrays.asList(rootLink), false);
        if (rootLink.isAfterRedirect()) {
            rootLink.setUrl(domain.getUrl());
        }
        parse(Arrays.asList(rootLink), 1);
        isWorking = false;
    }

    @Override
    public List<Link> getParsedLinks() {
        return parsedLinks;
    }

    @Override
    public Link getDomain() {
        return domain;
    }

    public void setWorking(boolean isWorking) {
        this.isWorking = isWorking;
    }

    public boolean isWorking() {
        return isWorking;
    }

    private void parse(List<Link> links, int level) {
        if (level > 3) {
            return;
        }
        parserStatus.setLevel(level);
        LOGGER.info("level " + level);

        List<Link> nextLevelLinks = getNextLevelLinks(links);
        List<Link> parseNext = filterLinks(nextLevelLinks, true);

        for (Link link : parseNext) {
            link.calculateOptions();
        }

        parsedLinks.addAll(parseNext);
        parse(parseNext, level + 1);
    }

    private List<Link> getNextLevelLinks(List<Link> links) {
        List<Link> nextLevelLinks = new ArrayList<>();

        ThreadPoolTaskExecutor executor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");

        for (Link link : links) {
            if (!isWorking) {
                return nextLevelLinks;
            }
            NextLevelLinksParserTask nextLevelLinksParserTask = (NextLevelLinksParserTask)
                    applicationContext.getBean("nextLevelLinksParserTask");
            nextLevelLinksParserTask.setCurrentLink(link);
            nextLevelLinksParserTask.setNextLevelLinks(nextLevelLinks);
            executor.execute(nextLevelLinksParserTask);
        }

        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.shutdown();

        try {
            executor.getThreadPoolExecutor().awaitTermination(30, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return nextLevelLinks;
    }

    private List<Link> filterLinks(List<Link> links, boolean checkForDomain) {
        List<Link> filteredLinks = new ArrayList<>();

        ThreadPoolTaskExecutor executor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");

        for (Link link : links) {
            if (!isWorking) {
                return filteredLinks;
            }
            FilterLinksParserTask filterLinksParserTask = (FilterLinksParserTask)
                    applicationContext.getBean("filterLinksParserTask");
            filterLinksParserTask.setCurrentLink(link);
            filterLinksParserTask.setFilterLinks(filteredLinks);
            filterLinksParserTask.setUsedUrls(usedUrls);
            filterLinksParserTask.setDomain(domain);
            filterLinksParserTask.setCheckForDomain(checkForDomain);
            executor.execute(filterLinksParserTask);
        }

        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.shutdown();

        try {
            executor.getThreadPoolExecutor().awaitTermination(30, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return filteredLinks;
    }
}
