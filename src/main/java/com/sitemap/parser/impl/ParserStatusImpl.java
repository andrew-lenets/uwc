package com.sitemap.parser.impl;

import com.sitemap.parser.ParserStatus;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * mocker on 27.03.15 at 10:50.
 */
@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.DEFAULT)
public class ParserStatusImpl implements ParserStatus {
    private int level = 1;
    private int allLinks = 0;
    private int processedLinks = 0;
    private int foundLinks = 0;
    private String currentUrl = "";

    @Override
    public void init() {
        allLinks = 1;
        processedLinks = 0;
        foundLinks = 0;
        level = 1;
    }

    @Override
    public synchronized void addAllLinks(int n) {
        allLinks += n;
    }

    @Override
    public synchronized void addProcessedLinks(int n) {
        processedLinks += n;
    }

    @Override
    public synchronized void addFoundLinks(int n) {
        foundLinks += n;
    }

    @Override
    public int getAllLinks() {
        return allLinks;
    }

    @Override
    public int getProcessedLinks() {
        return processedLinks;
    }

    @Override
    public int getFoundLinks() {
        return foundLinks;
    }

    @Override
    public String getCurrentUrl() {
        return currentUrl;
    }

    @Override
    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}