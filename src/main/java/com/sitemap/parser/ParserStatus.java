package com.sitemap.parser;

/**
 * mocker on 27.03.15 at 2:56.
 */
public interface ParserStatus {
    int getAllLinks();

    int getProcessedLinks();

    int getFoundLinks();

    void init();

    void addAllLinks(int n);

    void addProcessedLinks(int n);

    void addFoundLinks(int n);

    void setCurrentUrl(String currentUrl);

    String getCurrentUrl();

    public int getLevel();

    public void setLevel(int level);
}
