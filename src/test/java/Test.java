import com.sitemap.bo.ChangeFreq;
import com.sitemap.bo.HttpResponseWrapper;
import com.sitemap.bo.sitemap.Link;
import com.sitemap.bo.sitemap.Sitemap;
import com.sitemap.config.ApplicationConfig;
import com.sitemap.generator.Generator;
import com.sitemap.parser.Parser;
import com.sitemap.parser.ParserStatus;
import com.sitemap.utils.HttpRequestUtils;
import com.sun.xml.bind.marshaller.CharacterEscapeHandler;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * mocker on 24.03.15 at 0:49.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class})
public class Test {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private Generator generator;

    @Autowired
    private ParserStatus parserStatus;

    @Autowired
    private Parser parser;

    @org.junit.Test
    public void testName() throws Exception {

        final String s = "http://stackoverflow.com/";
        String path = "/home/mocker/workspace/uwc7/src/main/webapp/WEB-INF";
        generator.setPath(path);
        generator.generate(s);


//        while (true) {
//            System.out.println(parserStatus.getAllLinks());
//            System.out.println(parserStatus.getProcessedLinks());
//            System.out.println(parserStatus.getFoundLinks());
//            System.out.println(parserStatus.getCurrentUrl());
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        int y = 0;
    }

    @org.junit.Test
    public void testEncode() throws Exception {
        String url = "https://ru.wikipedia.org/wiki/Заглавная_страница?param1=1&параметр2=2&param=3dc r";
        url = url.replaceAll("\\s", "%20");
        URI uri = new URI(url);
        System.out.println(uri.toASCIIString());
        System.out.println(new String(url.getBytes()));
    }

    @org.junit.Test
    public void testExec() throws Exception {
        ThreadPoolTaskExecutor executor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");
        executor.execute(new T());
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.shutdown();
        executor.getThreadPoolExecutor().awaitTermination(30, TimeUnit.SECONDS);
        System.out.println("main finished");
        executor = (ThreadPoolTaskExecutor) applicationContext.getBean("taskExecutor");
        executor.execute(new T());
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.shutdown();
        executor.getThreadPoolExecutor().awaitTermination(30, TimeUnit.SECONDS);
    }

    class T implements Runnable {

        @Override
        public void run() {
            try {
                System.out.println("task started");
                Thread.sleep(5200);
                System.out.println("task finished");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @org.junit.Test
    public void testGenerator() throws Exception {
        System.out.println(ChangeFreq.ALWAYS);
        String path = "/home/mocker/workspace/uwc7/src/main/webapp/WEB-INF";
        generator.setPath(path);
        generator.generate("http://www.sitemaps.org/");
        int y = 0;
    }

    @org.junit.Test
    public void testName2() throws Exception {
        final String s = "https://vk.com";
        parser.init();
        parser.parse(s);
        List<Link> linkList = parser.getParsedLinks();
        for (Link link : linkList) {
            System.out.println(link);
        }
        System.out.println(linkList.size());
    }

    @org.junit.Test
    public void testMarshalling() throws Exception {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Sitemap.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            marshaller.setProperty("com.sun.xml.bind.characterEscapeHandler", new CharacterEscapeHandler() {
                @Override
                public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
                    out.write(ch, start, length);
                }
            });

            Sitemap sitemap = new Sitemap();
            sitemap.addLink(new Link("wwwe&amp;dc dcd'ffdcd\"fvfd&lt;&gt;"));
            marshaller.marshal(sitemap, System.out);
        } catch (JAXBException e) {
        }

    }

    @Autowired
    private HttpRequestUtils httpRequestUtils;

    @org.junit.Test
    public void testReq() throws Exception {
        HttpResponseWrapper httpResponseWrapper = httpRequestUtils.getResponse(new Link("https://www.ukr.net/news/jekonomika.html"));
        int y = 0;

    }
}
